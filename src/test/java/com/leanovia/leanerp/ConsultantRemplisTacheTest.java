package com.leanovia.leanerp;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;


public class ConsultantRemplisTacheTest {

        //WebDriverWait wait = new WebDriverWait(driver,30);
    //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'COMPOSE')]")));

    @Test
    public void RemplisTacheTest(){

        DesiredCapabilities capability = new DesiredCapabilities();
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(Const.HUB_ADDRESS+"/wd/hub"), capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));

        driver.get(Const.APP_ADRESS);

        WebElement mail = driver.findElement(By.id("username"));
        WebElement mdp = driver.findElement(By.id("password"));


        mail.sendKeys("bruno.da-silva@leanovia.com");
        mdp.sendKeys("test");

        WebElement login = driver.findElement(By.id("kc-login"));
        login.click();

        WebElement clickttk = driver.findElement(By.linkText("Time Tracker"));
        clickttk.click();


/// Si le consulant doit ajouter la tache lui même
    WebElement addtask = driver.findElement(By.xpath("//tr/td/div/span[contains(@class,'plus')]"));
    addtask.click();       
        
        Select client =   new Select(driver.findElement(By.xpath("//select[contains(@id,'client')]"))); //driver.findElement(By.xpath("//select/option[1]"));
        client.selectByIndex(0);

        Select projet =   new Select(driver.findElement(By.xpath("//select[contains(@id,'projet')]"))); //driver.findElement(By.xpath("//select/option[1]"));
        projet.selectByIndex(0);

        Select tache =   new Select(driver.findElement(By.xpath("//select[contains(@id,'tache')]"))); //driver.findElement(By.xpath("//select/option[1]"));
        tache.selectByIndex(0);
        
    WebElement submitTask = driver.findElement(By.xpath("//a[contains(@id,'sData')]"));
    submitTask.click();


    WebElement save0 = driver.findElement(By.id("btnSend"));
    
    save0.click();

    WebElement confirm0 = driver.findElement(By.xpath("//span[text()='Confirmer la saisie']"));

    confirm0.click();
    
///


    for (Integer i = 4; i <= 34; i++) { //Remplis la tache qui vient de lui être assigné
 

        WebElement element = driver.findElement(By.xpath("//td[@title='Etude']//following-sibling::td["+i.toString()+"]"));

        //System.out.println(element.);
    
            Actions actions = new Actions(driver);
    
            actions.doubleClick(element).perform();

           try {

            Select elementdd =   new Select(driver.findElement(By.xpath("//select[contains(@name,'_')]"))); //driver.findElement(By.xpath("//select/option[1]"));
            elementdd.selectByIndex(0);
     

           } catch (Exception e) {
               //TODO: handle exception
           }

        }

        WebElement save = driver.findElement(By.id("btnSend"));
        save.click();
 
        WebElement confirm = driver.findElement(By.xpath("//span[text()='Confirmer la saisie']"));
        confirm.click();

        driver.quit();


    }
    
}

package com.leanovia.leanerp;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;

// import org.openqa.selenium.interactions.Actions;
// import org.openqa.selenium.support.ui.Select;
// import org.openqa.selenium.support.ui.ExpectedConditions;
// import org.openqa.selenium.support.ui.WebDriverWait;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class VisiteSimpleTest {

    @Test
    public void VisiteSimpleProdChrome(){

        for (Integer c = 1; c <= 1; c++) {

            // ChromeOptions options = new ChromeOptions();
            // options.addArguments("--headless");

            // DesiredCapabilities capability = new DesiredCapabilities();
            
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            options.addArguments("--ignore-certificate-errors");
            options.addArguments("--remote-debugging-port=9222");
            options.addArguments("--no-sandbox");
           // options.addArguments("--disable-gpu ");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--disable-setuid-sandbox");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            options.merge(capabilities);
            
            WebDriver driver = null;
            try {
                driver = new RemoteWebDriver(new URL(Const.HUB_ADDRESS+"/wd/hub"), capabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            driver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));
            driver.get(Const.APP_ADRESS);
            WebElement mail = driver.findElement(By.id("username"));
            WebElement mdp = driver.findElement(By.id("password"));


            mail.sendKeys("synthetic.monitoring@leanovia.com");
            mdp.sendKeys("9@uoV/uUp~o3`)N^");

            try {

                WebElement login = driver.findElement(By.id("kc-login"));
                login.click();

                WebElement clickttk = driver.findElement(By.linkText("Time Tracker"));
                clickttk.click();

                WebElement clickprev = driver.findElement(By.xpath("//td[contains(@title,'Previous month')]"));


                for (Integer cl = 1; cl <= 13; cl++) {

                    System.out.println(cl);
                    clickprev.click();
                }

                 driver.quit();


            
        } catch (Exception e) {
            //TODO: handle exception
            driver.quit();

        }


      }

    }

@Test
public void VisiteSimpleProdFirefox(){

    for (Integer c = 1; c <= 1; c++) {

        // ChromeOptions options = new ChromeOptions();
        // options.addArguments("--headless");

        // DesiredCapabilities capability = new DesiredCapabilities();

        FirefoxOptions options = new FirefoxOptions();
        //ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--ignore-certificate-errors");
       // options.addArguments("--remote-debugging-port=9222");
        options.addArguments("--no-sandbox");
       // options.addArguments("--disable-gpu ");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-setuid-sandbox");
       // DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setCapability(FirefoxOptions.CAPA, options);
        //options.merge(capabilities);
        
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(Const.HUB_ADDRESS+"/wd/hub"), options);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));
        driver.get(Const.APP_ADRESS);
        WebElement mail = driver.findElement(By.id("username"));
        WebElement mdp = driver.findElement(By.id("password"));


        mail.sendKeys("synthetic.monitoring@leanovia.com");
        mdp.sendKeys("9@uoV/uUp~o3`)N^");

        try {

            WebElement login = driver.findElement(By.id("kc-login"));
            login.click();

            WebElement clickttk = driver.findElement(By.linkText("Time Tracker"));
            clickttk.click();

            WebElement clickprev = driver.findElement(By.xpath("//td[contains(@title,'Previous month')]"));


            for (Integer cl = 1; cl <= 13; cl++) {

                System.out.println(cl);
                clickprev.click();
            }

             driver.quit();


        
    } catch (Exception e) {
        //TODO: handle exception
        driver.quit();

    }


  }

}
}
//     public void VisiteSimple(){

//         for (Integer c = 1; c <= 5; c++) {

//             //DesiredCapabilities capability = new DesiredCapabilities();
//             //WebDriver driver = null;


//             // ChromeOptions options = new ChromeOptions();
//             // options.addArguments("--headless");

//             // DesiredCapabilities capability = new DesiredCapabilities();
            
//         ChromeOptions options = new ChromeOptions();
//         options.addArguments("--headless");
//         options.addArguments("--ignore-certificate-errors");
//         options.addArguments("--remote-debugging-port=9222");
//         options.addArguments("--no-sandbox");
//        // options.addArguments("--disable-gpu ");
//         options.addArguments("--disable-dev-shm-usage");
//         options.addArguments("--disable-setuid-sandbox");
//         //options.setBrowserVersion("95.0.4638.54");
//         DesiredCapabilities capabilities = new DesiredCapabilities();
//         capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//         options.merge(capabilities);
            
//             WebDriver driver = null;
//             try {
//                 driver = new RemoteWebDriver(new URL(Const.HUB_ADDRESS+"/wd/hub"), capabilities);
//             } catch (MalformedURLException e) {
//                 e.printStackTrace();
//             }

//             driver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));
//             driver.get("http://172.20.10.4:8090/" );
//             WebElement mail = driver.findElement(By.id("username"));
//             WebElement mdp = driver.findElement(By.id("password"));


//             mail.sendKeys("consultant"+c.toString()+".consultant@leanovia.com");
//             mdp.sendKeys("test");


//             try {

//                 WebElement login = driver.findElement(By.id("kc-login"));
//                 login.click();

//                 WebElement clickttk = driver.findElement(By.linkText("Time Tracker"));
//                 clickttk.click();

//                 WebElement clickprev = driver.findElement(By.xpath("//td[contains(@title,'Previous month')]"));


//                 for (Integer cl = 1; cl <= 13; cl++) {

//                     System.out.println(cl);
//                     clickprev.click();
//                 }

//                  driver.quit();

            
//         } catch (Exception e) {
//             //TODO: handle exception
//             driver.quit();

//         }


//       }

//     }
    
// }



 //WebDriverWait wait = new WebDriverWait(driver,30);
 //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'COMPOSE')]")));
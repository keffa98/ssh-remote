package com.leanovia.leanerp;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.remote.RemoteWebDriver;


public class AdminTest {   

    @Test
    public void Admintest() {
        DesiredCapabilities capability = new DesiredCapabilities();
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(Const.HUB_ADDRESS+"/wd/hub"), capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));

        driver.get(Const.APP_ADRESS);

        WebElement mail = driver.findElement(By.id("username"));
        WebElement mdp = driver.findElement(By.id("password"));

        mail.sendKeys("consultant10.consultant@leanovia.com");
        mdp.sendKeys("test");

        try {
            WebElement login = driver.findElement(By.id("kc-login"));
            login.click();


             WebElement clickttk = driver.findElement(By.linkText("Time Tracker"));
             clickttk.click();

        WebElement admintms = driver.findElement(By.xpath("//a[@href ='/timetracker/admin/consultants/0']"));
        admintms.click();

        // WebElement next1 = driver.findElement(By.xpath("//a[@href ='./1']"));
        // next1.click();

        // WebElement next2 = driver.findElement(By.xpath("//a[@href ='./2']"));
        // next2.click();

        // WebElement next3 = driver.findElement(By.xpath("//a[@href ='./3']"));
        // next3.click();

        //a[@href ='/timetracker/admin/consultation/@Consulant.Name']
        WebElement consultantTimesheet = driver.findElement(By.xpath("//a[@href ='/timetracker/admin/consultation/Dasilva']")); //Voir la timesheet du consultant
        consultantTimesheet.click();

        WebElement addtask = driver.findElement(By.xpath("//tr/td/div/span[contains(@class,'plus')]")); // click sur le bouton add
        addtask.click();       
        
        Select client =   new Select(driver.findElement(By.xpath("//select[contains(@id,'client')]"))); //selection du client
        client.selectByIndex(0);

        Select projet =   new Select(driver.findElement(By.xpath("//select[contains(@id,'projet')]"))); //selection du projet
        projet.selectByIndex(0);

        Select tache =   new Select(driver.findElement(By.xpath("//select[contains(@id,'tache')]"))); //selection du projet
        tache.selectByIndex(0);
        
        WebElement submitTask = driver.findElement(By.xpath("//a[contains(@id,'sData')]")); //Sauvegarder l'ajout tâche
        submitTask.click();




        WebElement save = driver.findElement(By.id("btnSend")); //Enregistrer
        save.click();
   
        WebElement confirm = driver.findElement(By.xpath("//span[text()='Confirmer la saisie']")); //Confirmer la saisie
        confirm.click();

        // WebElement clickprev = driver.findElement(By.xpath("//td[contains(@title,'Previous month')]"));
        // clickprev.click();

        // for (Integer c = 1; c <= 13; c++) {
    
        //     System.out.println(c);
        //     clickprev.click();
        // }



        driver.quit();


        } catch (Exception e) {
        //TODO: handle exception
        driver.quit();
        }    
        


        //driver.quit();
    }



   




}

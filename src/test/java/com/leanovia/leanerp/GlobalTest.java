package com.leanovia.leanerp;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;


public class GlobalTest {

    @Test
    public void runAllTests() {
        // Class<?>[] classes = { VisiteSimpleTest.class, VisiteSimpleTest.class, VisiteSimpleEtRemplisTacheTest.class, VisiteSimpleEtRemplisTacheTest.class };
        Class<?>[] classes = { NouvelleTacheEtRemplissageNouvelleTacheTtk.class, NouvelleTacheEtRemplissageNouvelleTacheTtk.class, VisiteSimpleTest.class, VisiteSimpleTest.class };

        // ParallelComputer(true,true) will run all classes and methods
        // in parallel.  (First arg for classes, second arg for methods)
        JUnitCore.runClasses(new ParallelComputer(true, true), classes);
    }

}